<?php

namespace Alik75\VoyagerVisualComposer;

use Alik75\VoyagerVisualComposer\CustomFields\VisualComposer;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

class VisualComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        // publish config
        $this->publishes([
            __DIR__.'/../publishable/config/visual-composer.php' => config_path('visual-composer.php'),
        ]);

        // include __DIR__.'/routes.php';
        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        // add views
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'VisualComposer');

        // publish assets
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../publishable/assets' => public_path('vc-assets'),
            ], 'public');
        }
        

        // translate
        $this->loadTranslationsFrom(__DIR__ . '/../publishable/lang', 'visualcomposer');
        
        config(['voyager.additional_css' => array_merge(config('voyager.additional_css'),[
            'vc-assets/css/visual-composer.css',
            'vc-assets/css/dropzone.min.css',
            'vc-assets/css/dragula.min.css',
        ])]);
        
        config(['voyager.additional_js' => array_merge(config('voyager.additional_js'),[
            'vc-assets/js/dragula.min.js',
            'vc-assets/js/jscolor.js',
            'vc-assets/js/custom-tinymce-voyager.js',
        ])]);

        Voyager::addFormField(VisualComposer::class);
    }
}
