<?php

namespace Alik75\VoyagerVisualComposer\CustomFields;

use TCG\Voyager\FormFields\AbstractHandler;

class VisualComposer extends AbstractHandler
{
    protected $codename = 'visual_composer';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('VisualComposer::visual-composer.layout', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
}
