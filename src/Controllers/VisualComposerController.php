<?php

namespace Alik75\VoyagerVisualComposer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VisualComposerController extends Controller
{
    public function UploadFile(Request $request){

        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $fileName=sha1(time())."_vc\$_".$fileName;
        $file->move(config("visual-composer.upload_dir"),$fileName);
        return response()->json([
            "status_code"=>200,
            "message"=>"Success",
            "data"=>[
                "file_name"=>$fileName
            ]
        ], 200);
    }

    public function DeleteFile(Request $request){
        $fileName=$request->input('id');
        unlink(config("visual-composer.upload_dir")."/".$fileName);
        return response()->json([
            "status_code"=>200,
            "message"=>"Success",
            "data"=>[
                "file_name"=>$fileName
            ]
        ], 200);
    }
}
