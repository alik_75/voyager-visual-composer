<?php

use Alik75\VoyagerVisualComposer\Controllers\VisualComposerController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'admin/visual-composer'],function(){
    Route::post('upload',[VisualComposerController::class,'UploadFile'])->name('vc.dropzone.upload');
    Route::post('delete',[VisualComposerController::class,'DeleteFile'])->name('vc.dropzone.delete');
});