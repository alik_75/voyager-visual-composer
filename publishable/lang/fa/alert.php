<?php

return [
    'component_name'=> 'Slider Component',
    'type'=> 'نوع',
    'type_warning'=> 'هشدار',
    'type_danger'=> 'اخطار',
    'type_notice'=> 'نکته',
    'text'=> 'متن',
];