<?php

return [
    'component_name'=> 'کامپوننت فضای خالی',
    'height_label'=> 'ارتفاع (10px, 100% e.g.)',
    'height_placeholder'=> 'ارتفاع را وارد کنید',
];