<?php

return [
    'component_name'=> 'کامپوننت دکمه',
    'text_label'  => 'متن',
    'text_placeholder'  => 'متن دکمه را وارد کنید',
    'link_label'  => 'لینک',
    'link_placeholder'  => 'لینک دکمه را وارد کنید',
    'type_label'  => 'نوع',
    'action_label'  => 'عملکرد',
    'action_type_self_target' => 'در همان تب',
    'action_type_newtab_target' => 'تب جدید',
];