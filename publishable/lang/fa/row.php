<?php

return [
    'component_name'=> 'تنظیمات ردیف',
    'title_label'=> 'عنوان',
    'title_placeholder'=> 'عنوان ردیف را وارد کنید',
    'direction'=> 'جهت محتوی',
    'has_container'=> 'درون کانتینر باشد',
    'has_line'=> 'دارای خط',
    'background_color'=> 'رنگ پس زمینه',
    

];