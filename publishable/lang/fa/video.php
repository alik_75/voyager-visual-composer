<?php

return [
    'component_name'=> 'کامپوننت ویدیو',
    'height_label'=> 'ارتفاع (%)',
    'height_placeholder'=> 'ارتفاع (برای 100% فیلد را خالی بگذارید)',
    'width_label'=> 'عرض (%)',
    'width_placeholder'=> 'عرض (برای 100% فیلد را خالی بگذارید)',
    'url'=> 'URL',
    'upload'=> 'بارگذاری',
    'video_type'=> 'نوع ویدیو',

];