<?php

return [
    'component_name'=> 'Slider Component',
    'type'=> 'Type',
    'type_warning'=> 'Warning',
    'type_danger'=> 'Danger',
    'type_notice'=> 'Notice',
    'text'=> 'Text',
];