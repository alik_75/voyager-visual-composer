<?php

return [
    'component_name'=> 'Empty Space Component',
    'height_label'=> 'Height (10px, 100% e.g.)',
    'height_placeholder'=> 'Enter Height',
];