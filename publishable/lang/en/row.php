<?php

return [
    'component_name'=> 'Row Settings',
    'title_label'=> 'Title',
    'title_placeholder'=> 'Enter Row Title',
    'direction'=> 'Content Direction',
    'has_container'=> 'Inside the container',
    'has_line'=> 'With a line',
    'background_color'=> 'Background Color',


];