<?php

return [
    'component_name'=> 'Code Component',
    'text_label'=> 'Code',
    'text_placeholder'=> 'Enter your code',
];