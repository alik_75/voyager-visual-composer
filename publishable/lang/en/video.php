<?php

return [
    'component_name'=> 'Video Component',
    'height_label'=> 'Height (%)',
    'height_placeholder'=> 'Height (empty for 100%)',
    'width_label'=> 'Width (%)',
    'width_placeholder'=> 'Width (empty for 100%)',
    'url'=> 'URL',
    'upload'=> 'Upload',
    'video_type'=> 'Video Type',
];