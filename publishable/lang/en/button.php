<?php

return [
    'component_name'=> 'Button Component',
    'text_label'  => 'Text',
    'text_placeholder'  => 'Enter Text',
    'link_label'  => 'Link',
    'link_placeholder'  => 'Enter URL',
    'type_label'  => 'Type',
    'action_label'  => 'Action',
    'action_type_self_target' => 'Self',
    'action_type_newtab_target' => 'New Tab',
];