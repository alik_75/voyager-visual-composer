<div class="modal-vc modal-vc-empty" onclick="onCloseVCModal(this);">
    <div class="modal-dialog-vc" onclick="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::empty-space.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::empty-space.height_label') }}</label>
                <input class="vc-input" name="empty-height"
                    placeholder="{{ __('visualcomposer::empty-space.height_placeholder') }}" />
            </div>


        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
