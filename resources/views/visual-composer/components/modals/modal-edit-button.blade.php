<div class="modal-vc modal-vc-button" onclick="onCloseVCModal(this);">
    <div class="modal-dialog-vc" onclick="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::button.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">

            <div class="vc-form-group">
                <label>{{ __('visualcomposer::button.text_label') }}</label>
                <input class="vc-input" dir="auto" name="btn-text"
                    placeholder="{{ __('visualcomposer::button.text_placeholder') }}" />
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::button.link_label') }}</label>
                <input class="vc-input" dir="auto" name="btn-link"
                    placeholder="{{ __('visualcomposer::button.link_placeholder') }}" />
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::button.type_label') }}</label>
                <select class="vc-input" name="btn-type">
                    <option value="primary">Primary</option>
                    <option value="secondary">Secondary</option>
                    <option value="success">Success</option>
                    <option value="danger">Danger</option>
                    <option value="warning">Warning</option>
                    <option value="info">Info</option>
                    <option value="disabled">Disabled</option>
                </select>
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::button.action_label') }}</label>
                <select class="vc-input" name="btn-target">
                    <option value="_blank">{{ __('visualcomposer::button.action_type_self_target') }}</option>
                    <option value="_self">{{ __('visualcomposer::button.action_type_newtab_target') }}</option>

                </select>
            </div>
        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
