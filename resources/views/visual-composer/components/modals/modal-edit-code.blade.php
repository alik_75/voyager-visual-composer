<div class="modal-vc modal-vc-code" onclick="onCloseVCModal(this);">
    <div class="modal-dialog-vc" onclick="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::code.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::code.text_label') }}</label>
                <textarea name="vc-code" rows="8" class="text-box"
                    placeholder="{{ __('visualcomposer::code.text_placeholder') }}"></textarea>
            </div>
        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
