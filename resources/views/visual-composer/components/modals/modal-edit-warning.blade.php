<div class="modal-vc modal-vc-warning" onclick="onCloseVCModal(this);">
    <div class="modal-dialog-vc" onclick="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::alert.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::alert.type') }}</label>
                <select class="vc-input" name="warning-type">
                    <option value="warning">{{ __('visualcomposer::alert.type_warning') }}</option>
                    <option value="danger">{{ __('visualcomposer::alert.type_danger') }}</option>
                    <option value="notice">{{ __('visualcomposer::alert.type_notice') }}</option>
                </select>
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::alert.text') }}</label>
                <textarea name="vc-warning" rows="8" class="text-box"></textarea>
            </div>
        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
