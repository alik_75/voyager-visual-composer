<div class="modal-vc modal-vc-row" onclick="onCloseVCModal(this);">
    <div class="modal-dialog-vc" onclick="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::row.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::row.title_label') }}</label>
                <input class="vc-input" name="row-title"
                    placeholder="{{ __('visualcomposer::row.title_placeholder') }}" />
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::row.direction') }}</label>
                <br>
                <div>
                    <input type="radio" id="right" name="row-direction" checked value="right">
                    <label for="right"><svg xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in"
                            height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 127.65 127.03">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: black
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <path class="fil0"
                                    d="M127.65 25.14l0 76.74c0,13.85 -11.3,25.14 -25.14,25.14l-77.37 0c-13.85,0 -25.14,-11.3 -25.14,-25.14l0 -76.74c0,-13.85 11.3,-25.14 25.14,-25.14l77.37 0c13.85,0 25.14,11.3 25.14,25.14zm-79.33 12.53c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-28.1 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l28.1 0zm0 45.86c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-28.1 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l28.1 0zm17.84 -51.47l34.3 0c3.57,0 6.49,2.92 6.49,6.49l0 49.91c0,3.57 -2.92,6.49 -6.49,6.49l-34.3 0c-3.57,0 -6.49,-2.92 -6.49,-6.49l0 -49.91c0,-3.57 2.92,-6.49 6.49,-6.49zm52.76 69.82l0 -76.74c0,-4.51 -1.85,-8.62 -4.82,-11.59 -2.98,-2.98 -7.08,-4.82 -11.59,-4.82l-77.37 0c-4.51,0 -8.62,1.85 -11.59,4.82 -2.98,2.98 -4.82,7.08 -4.82,11.59l0 76.74c0,4.51 1.85,8.62 4.82,11.59 2.98,2.98 7.08,4.82 11.59,4.82l77.37 0c4.51,0 8.62,-1.85 11.59,-4.82 2.98,-2.98 4.82,-7.08 4.82,-11.59z" />
                            </g>
                        </svg></label>
                    <input type="radio" id="center" name="row-direction" value="center">
                    <label for="center"><svg xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in"
                            height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 127.65 127.03">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: black
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <path class="fil0"
                                    d="M127.65 25.14l0 76.74c0,13.85 -11.3,25.14 -25.14,25.14l-77.37 0c-13.85,0 -25.14,-11.3 -25.14,-25.14l0 -76.74c0,-13.85 11.3,-25.14 25.14,-25.14l77.37 0c13.85,0 25.14,11.3 25.14,25.14zm-16.07 58.39c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-15.74 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l15.74 0zm0 -45.86c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-15.74 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l15.74 0zm-79.58 45.86c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-15.74 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l15.74 0zm0 -45.86c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-15.74 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l15.74 0zm14.59 -5.61l34.46 0c3.53,0 6.41,2.89 6.41,6.41l0 50.07c0,3.53 -2.89,6.41 -6.41,6.41l-34.46 0c-3.53,0 -6.41,-2.89 -6.41,-6.41l0 -50.07c0,-3.53 2.89,-6.41 6.41,-6.41zm72.33 69.82l0 -76.74c0,-4.51 -1.85,-8.62 -4.82,-11.59 -2.98,-2.98 -7.08,-4.82 -11.59,-4.82l-77.37 0c-4.51,0 -8.62,1.85 -11.59,4.82 -2.98,2.98 -4.82,7.08 -4.82,11.59l0 76.74c0,4.51 1.85,8.62 4.82,11.59 2.98,2.98 7.08,4.82 11.59,4.82l77.37 0c4.51,0 8.62,-1.85 11.59,-4.82 2.98,-2.98 4.82,-7.08 4.82,-11.59z" />
                            </g>
                        </svg></label>
                    <input type="radio" id="left" name="row-direction" value="left">
                    <label for="left"><svg xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in"
                            height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 127.65 127.03">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: black
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <path class="fil0"
                                    d="M127.65 25.14l0 76.74c0,13.85 -11.3,25.14 -25.14,25.14l-77.37 0c-13.85,0 -25.14,-11.3 -25.14,-25.14l0 -76.74c0,-13.85 11.3,-25.14 25.14,-25.14l77.37 0c13.85,0 25.14,11.3 25.14,25.14zm-20.2 58.39c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-28.1 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l28.1 0zm0 -45.86c1.61,0 2.91,1.3 2.91,2.91 0,1.61 -1.3,2.91 -2.91,2.91l-28.1 0c-1.61,0 -2.91,-1.3 -2.91,-2.91 0,-1.61 1.3,-2.91 2.91,-2.91l28.1 0zm-79.99 -5.61l34.46 0c3.53,0 6.41,2.89 6.41,6.41l0 50.07c0,3.53 -2.89,6.41 -6.41,6.41l-34.46 0c-3.53,0 -6.41,-2.89 -6.41,-6.41l0 -50.07c0,-3.53 2.89,-6.41 6.41,-6.41zm91.46 69.82l0 -76.74c0,-4.51 -1.85,-8.62 -4.82,-11.59 -2.98,-2.98 -7.08,-4.82 -11.59,-4.82l-77.37 0c-4.51,0 -8.62,1.85 -11.59,4.82 -2.98,2.98 -4.82,7.08 -4.82,11.59l0 76.74c0,4.51 1.85,8.62 4.82,11.59 2.98,2.98 7.08,4.82 11.59,4.82l77.37 0c4.51,0 8.62,-1.85 11.59,-4.82 2.98,-2.98 4.82,-7.08 4.82,-11.59z" />
                            </g>
                        </svg></label>
                </div>
            </div>
            <div class="vc-form-group">
                <input checked type="checkbox" id="row-has-container" name="row-has-container" />
                <label for="row-has-container">{{ __('visualcomposer::row.has_container') }}</label>
            </div>
            <div class="vc-form-group">
                <input checked type="checkbox" id="row-has-line" name="row-has-line" />
                <label for="row-has-line">{{ __('visualcomposer::row.has_line') }}</label>
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::row.background_color') }}</label>
                <input class="vc-input" data-jscolor="{}" name="row-bg-color" value="#ffffff" />
            </div>
        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onRowInfoModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
