<div class="modal-vc modal-vc-text" onclick="onCloseVCModal(this);">
    <div class="modal-dialog-vc" onclick="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::text-editor.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">
            <textarea id="vc-text-editor"></textarea>
        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
