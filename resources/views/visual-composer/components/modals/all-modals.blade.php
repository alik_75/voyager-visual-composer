@include('VisualComposer::visual-composer.components.modals.modal-edit-text')
@include('VisualComposer::visual-composer.components.modals.modal-edit-button')
@include('VisualComposer::visual-composer.components.modals.modal-edit-image')
@include('VisualComposer::visual-composer.components.modals.modal-edit-video')
@include('VisualComposer::visual-composer.components.modals.modal-edit-empty')
@include('VisualComposer::visual-composer.components.modals.modal-edit-warning')
@include('VisualComposer::visual-composer.components.modals.modal-edit-slider')
@include('VisualComposer::visual-composer.components.modals.modal-edit-code')
@include('VisualComposer::visual-composer.components.modals.modal-edit-row')


<script>

    document.querySelectorAll(".modal-vc").forEach(item => document.body.append(item));
    var dropzoneImage = null;
    var dropzoneVideo = null;
    var dropzoneSlider=null;
    
    function ChangeComponentData(elem) {
        let component=elem.closest(".component-item");
        let type=component.getAttribute("data-type");
        let data=elem.getAttribute("value");
        if(data=="")
            return;


        data=JSON.parse(data);

        let optionText="";
        switch (type) {
            case "text":

                break;
            case "button":

                break;
            case "image":
                optionText="Width:"+data.width+", Height:"+data.height;
                break;
            case "empty":

                break;
            case "warning":

                break;
            case "code":

                break;
            case "slider":

                break;
        }
    }

    function onCloseVCModal(elem) {
        elem.closest(".modal-vc").classList.remove("open");
        document.body.classList.remove("open-modal");
    }

    function modalClick(e) {
        e.stopPropagation();
        console.log("body click")
    }

    function validateImage(modal) {
        let imgWidth = modal.querySelector("input[name='img-width']").value;
        let imgHeight = modal.querySelector("input[name='img-height']").value;
        let imgWidthType = modal.querySelector("select[name='img-width-type']").value;
        let imgHeightType = modal.querySelector("select[name='img-height-type']").value;
        let imgAlt = modal.querySelector("input[name='img-alt']").value;
        if (!(imgWidthType=="auto" || (imgWidth != "" && /^[0-9]*$/.test(imgWidth)))) {
            toastr.error("Image width should be a number");
            return false;
        }
        if (!(imgHeightType=="auto" || (imgHeight != "" && /^[0-9]*$/.test(imgHeight)))) {
            toastr.error("Image height should be a number");
            return false;
        }
        if(!((imgWidthType=="auto" || (parseInt(imgWidth)>0 && parseInt(imgWidth)<=100)) || (imgHeightType=="auto" || (parseInt(imgHeight)>0 && parseInt(imgHeight)<=100)))){
            toastr.error("Image Width & Height range is [0-100]");
            return false;
        }
        if (imgAlt == "") {
            toastr.error("Image alt can not empty");
            return false;
        }
        return true;
    }

    function onModalSubmit(elem) {
        let modal = elem.closest(".modal-vc");
        let type = modal.getAttribute("data-type");
        let callerId = modal.getAttribute("data-caller-id");
        let data = {};
        switch (type) {
            case "text":
                data = getTextModalData(modal);
                break;
            case "button":
                data = getButtonModalData(modal);
                break;
            case "image":
                if (!validateImage(modal)) {
                    return;
                }
                data = getImageModalData(modal);
                break;
            case "video":
                data = getVideoModalData(modal);
                break;
            case "empty":
                data = getEmptyModalData(modal);
                break;
            case "warning":
                data = getWarningModalData(modal);
                break;
            case "code":
                data = getCodeModalData(modal);
                break;
            case "slider":
                data = getSliderModalData(modal);
                break;
        }
        let callerElem = document.querySelector(".vc-container .component-item[data-id='" + callerId + "']");
        callerElem.querySelector(".item-info input[name='component-data']").setAttribute("value", JSON.stringify(data));
        onCloseVCModal(elem);
        saveInfoVC();
    }

    function onOpenVCModal(elem) {
        let componentType = elem.closest(".component-item").getAttribute("data-type");
        let dataId = elem.closest(".component-item").getAttribute("data-id");
        let data = elem.closest(".component-item").querySelector(".item-info input[name='component-data']").value;
        if (data == "")
            data = "{}";
        data = JSON.parse(data);
        if (componentType == "text") {
            initModalTextTinyMCE(data);
            setTextModalData(data);
        } else if (componentType == "button") {
            setButtonModalData(data);
        } else if (componentType == "image") {
            initModalImageDropzone();
            setImageModalData(data);
        } else if (componentType == "video") {
            initModalVideoDropzone();
            setVideoModalData(data);
        } else if (componentType == "empty") {
            setEmptyModalData(data);
        } else if (componentType == "warning") {
            setWarningModalData(data);
        } else if (componentType == "code") {
            setCodeModalData(data);
        } else if (componentType == "slider") {
            initModalSliderDropzone();
            setSliderModalData(data);
        }

        document.querySelector(".modal-vc.modal-vc-" + componentType).classList.add("open");
        document.querySelector(".modal-vc.modal-vc-" + componentType).setAttribute("data-type", componentType);
        document.querySelector(".modal-vc.modal-vc-" + componentType).setAttribute("data-caller-id", dataId);
        document.body.classList.add("open-modal");
    }

    function onCopyComponent(elem) {
        let componentContainer = elem.closest(".vc-column-container");
        let copyElem = elem.closest(".component-item").cloneNode(true);
        let elemId = elem.closest(".component-item").getAttribute("data-id");
        copyElem.setAttribute("data-id", elemId.split("_").slice(0, elemId.split("_").length - 1).join("_") + "_" + (componentContainer.childElementCount + 1));
        componentContainer.append(copyElem);

        saveInfoVC();
    }

    function openRowInfoModal(elem) {
        let row = elem.closest(".vc-row");
        let dataId = row.getAttribute("data-id");
        let data = row.querySelector("input[name='row-data']").value;
        if (data == "")
            data = "{}";
        data = JSON.parse(data);
        let modal = document.querySelector(".modal-vc.modal-vc-row");

        modal.querySelector("input[name='row-title']").value = data.title ? data.title : "";
        modal.querySelector("input[type='radio'][name='row-direction'][value='" + (data.direction ? data.direction : "right") + "']").checked = true;
        modal.querySelector("input[name='row-has-container']").checked = Object.keys(data).indexOf("has_container") != -1 ? data.has_container : false;
        modal.querySelector("input[name='row-has-line']").checked = Object.keys(data).indexOf("has_line") != -1 ? data.has_line : false;
        modal.querySelector("input[name='row-bg-color']").value = data.bg_color ? data.bg_color : "#ffffff";
        modal.querySelector("input[name='row-bg-color']").jscolor.fromString(data.bg_color ? data.bg_color : "#ffffff");
        modal.classList.add("open");
        modal.setAttribute("data-caller-id", dataId);
        document.body.classList.add("open-modal");
    }

    function onRowInfoModalSubmit(elem) {
        let modal = elem.closest(".modal-vc");
        let callerId = modal.getAttribute("data-caller-id");
        let rowTitle = modal.querySelector("input[name='row-title']").value;
        let rowBgColor = modal.querySelector("input[name='row-bg-color']").value;
        var radios = modal.querySelectorAll('input[name="row-direction"]');
        let rowDir = "";
        for (let i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                rowDir = radios[i].value;
                break;
            }
        }
        let rowContainer = modal.querySelector("input[name='row-has-container']").checked;
        let rowLine = modal.querySelector("input[name='row-has-line']").checked;
        let data = {
            title: rowTitle,
            direction: rowDir,
            has_container: rowContainer,
            has_line: rowLine,
            bg_color: rowBgColor
        }
        let callerElem = document.querySelector(".vc-container .vc-row[data-id='" + callerId + "']");
        callerElem.querySelector("input[name='row-data']").setAttribute("value", JSON.stringify(data));
        // callerElem.querySelector(".item-info input[name='component-data']").setAttribute("value", JSON.stringify(data));
        onCloseVCModal(elem);
        saveInfoVC();
    }

    var startClickModal;
    var endClickModal;
    function modalOnMouseDown(modal) {
        startClickModal=Date.now();
    }

    function modalOnMouseUp(modal) {
        endClickModal = Date.now();
        let diff = (endClickModal - startClickModal) + 1;
        if (diff > 750) {

        }else{
            modal.classList.remove("open");
            document.body.classList.remove("open-modal");
        }
    }

    function widthHeightTypeChange(elem) {
        let input=elem.closest(".vc-form-group").querySelector("input.vc-input");
        if(elem.value=="auto"){
            input.setAttribute("disabled","disabled");
            input.value="";
        }else{
            input.removeAttribute("disabled");
        }
    }

    function videoTypeChanged(elem) {
        let type = elem.getAttribute("value");

        switch (type) {
            case "upload":
                // init dropzone
                // initModalVideoDropzone();
                elem.closest(".video-type-selector").querySelector(".vc-dropzone").style.display = "block";
                elem.closest(".video-type-selector").querySelector("input[name='video-url']").style.display = "none";
                break;
            case "url":
                // form input
                elem.closest(".video-type-selector").querySelector(".vc-dropzone").style.display = "none";
                elem.closest(".video-type-selector").querySelector("input[name='video-url']").style.display = "block";
                break;
            default:

        }
    }

    function initModalTextTinyMCE(data) {
        let additionalConfig = {
            selector: '.modal-vc textarea#vc-text-editor',
            // plugins: 'link, image, code, table, textcolor, lists,paste',
            // toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table | code',
            // image_caption: true,
            // image_title: true,
            // min_height: 600,
            // editor_selector : "vc-text-editor",
            // resize: 'vertical',
            // plugins: [
            //     "advlist autolink lists link image charmap print preview anchor",
            //     "searchreplace visualblocks code fullscreen",
            //     "insertdatetime media table paste",
            // ],
	    height: 400,
	    autoresize_min_height: 400,
	    autoresize_max_height: 800,
            init_instance_callback: function (inst) { inst.execCommand('mceAutoResize');setTextModalData(data); },
	    external_plugins: {
                'directionality': '/vc-assets/tinymce/plugins/directionality/plugin.min.js',
                'paste': '/vc-assets/tinymce/plugins/paste/plugin.min.js',
                'searchreplace': '/vc-assets/tinymce/plugins/searchreplace/plugin.min.js',
                // 'preview': '/vc-assets/tinymce/plugins/preview/plugin.min.js',
                // 'searchreplace': '/vc-assets/tinymce/plugins/searchreplace/plugin.min.js',
                // 'directionality': '/vc-assets/tinymce/plugins/directionality/plugin.min.js',
                // 'visualchars': '/vc-assets/tinymce/plugins/visualchars/plugin.min.js',
                // 'visualblocks': '/vc-assets/tinymce/plugins/visualblocks/plugin.min.js',
                // 'autoresize': '/vc-assets/tinymce/plugins/autoresize/plugin.min.js',
                // 'fullscreen': '/vc-assets/tinymce/plugins/fullscreen/plugin.min.js',
                // 'media': '/vc-assets/tinymce/plugins/media/plugin.min.js',
                // 'template': '/vc-assets/tinymce/plugins/template/plugin.min.js',
                // 'codesample': '/vc-assets/tinymce/plugins/codesample/plugin.min.js',
                // 'charmap': '/vc-assets/tinymce/plugins/charmap/plugin.min.js',
                // 'pagebreak': '/vc-assets/tinymce/plugins/pagebreak/plugin.min.js',
                // 'hr': '/vc-assets/tinymce/plugins/hr/plugin.min.js',
                // 'anchor': '/vc-assets/tinymce/plugins/anchor/plugin.min.js',
                // 'nonbreaking': '/vc-assets/tinymce/plugins/nonbreaking/plugin.min.js',
                // 'insertdatetime': '/vc-assets/tinymce/plugins/insertdatetime/plugin.min.js',
                // 'toc': '/vc-assets/tinymce/plugins/toc/plugin.min.js',
                // 'advlist': '/vc-assets/tinymce/plugins/advlist/plugin.min.js',
                // 'wordcount': '/vc-assets/tinymce/plugins/wordcount/plugin.min.js',
                // 'imagetools': '/vc-assets/tinymce/plugins/imagetools/plugin.min.js',
                // 'textpattern': '/vc-assets/tinymce/plugins/textpattern/plugin.min.js',
                // 'help': '/vc-assets/tinymce/plugins/help/plugin.min.js',
            },
            // toolbar:
            //     "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | ltr rtl",
	    
        }
        let configs=window.voyagerTinyMCE.getConfig(additionalConfig);
        configs.plugins+=", directionality, paste, searchreplace";
        configs.toolbar+=" | ltr rtl | removeformat | searchreplace";
        // configs.paste_as_text=true;
        configs.images_upload_url='https://lara.maxeo.io/visual_composer/uploads/postAcceptor.php';
        configs.automatic_uploads= true;
        //	configs.fullscreen_native= true;
        tinymce.init(configs);
    }

    function initModalImageDropzone() {
        document.querySelector(".modal-vc.modal-vc-image .vc-dropzone").innerHTML = '';
        var f = document.createElement("form");
        f.setAttribute('method', "post");
        f.setAttribute('action', "{{route('vc.dropzone.upload')}}");
        f.classList.add("dropzone");

        let csrfInp = document.createElement("input");
        csrfInp.setAttribute("type", "hidden");
        csrfInp.setAttribute("name", "_token");
        csrfInp.setAttribute("value", document.querySelector("meta[name='csrf-token']").getAttribute("content"));

        f.append(csrfInp);
        document.querySelector(".modal-vc.modal-vc-image .vc-dropzone").append(f);

        dropzoneImage = new Dropzone('.modal-vc.modal-vc-image .dropzone', {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            maxFiles: 1,
            addRemoveLinks: true,
            init: function () {
                this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
            },
            removedfile: function (file) {
                var name = file.serverName;
                fetch("{{route("vc.dropzone.delete")}}", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: name,
                        _token: document.querySelector("meta[name='csrf-token']").getAttribute("content")
                    })
                }).then(res => {

                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function (file, response) {
                file.serverName = response.data.file_name;
                try {
                    let alt = file.name.split(".").slice(0, file.name.split(".").length - 1).join(".").replaceAll("-", " ");
                    document.querySelector(".modal-vc.modal-vc-image input[name='img-alt']").setAttribute("value", alt);
                } catch (e) {

                }
            }
        });
    }

    function initModalVideoDropzone() {
        document.querySelector(".modal-vc.modal-vc-video .vc-dropzone").innerHTML = '';
        var f = document.createElement("form");
        f.setAttribute('method', "post");
        f.setAttribute('action', "{{route('vc.dropzone.upload')}}");
        f.classList.add("dropzone");

        let csrfInp = document.createElement("input");
        csrfInp.setAttribute("type", "hidden");
        csrfInp.setAttribute("name", "_token");
        csrfInp.setAttribute("value", document.querySelector("meta[name='csrf-token']").getAttribute("content"));

        f.append(csrfInp);
        document.querySelector(".modal-vc.modal-vc-video .vc-dropzone").append(f);

        dropzoneVideo = new Dropzone('.modal-vc.modal-vc-video .dropzone', {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 120, // MB
            maxFiles: 1,
            addRemoveLinks: true,
            init: function () {
                this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
                //add current files
                // var mockFile = { name: "myimage.jpg",serverName:"myimage.jpg", size: 12345, type: 'image/jpeg' };
                // this.addFile.call(this, mockFile);
                // this.options.thumbnail.call(this, mockFile, "http://someserver.com/myimage.jpg");
            },
            removedfile: function (file) {
                var name = file.serverName;
                fetch("{{route("vc.dropzone.delete")}}", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: name,
                        _token: document.querySelector("meta[name='csrf-token']").getAttribute("content")
                    })
                }).then(res => {

                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function (file, response) {
                file.serverName = response.data.file_name;
                try {
                    let alt = file.name.split(".").slice(0, file.name.split(".").length - 1).join(".").replaceAll("-", " ");
                } catch (e) {

                }
            }
        });
    }

    function initModalSliderDropzone() {
        document.querySelector(".modal-vc.modal-vc-slider .vc-dropzone").innerHTML = '';
        var f = document.createElement("form");
        f.setAttribute('method', "post");
        f.setAttribute('action', "{{route('vc.dropzone.upload')}}");
        f.classList.add("dropzone");
        let csrfInp = document.createElement("input");
        csrfInp.setAttribute("type", "hidden");
        csrfInp.setAttribute("name", "_token");
        csrfInp.setAttribute("value", document.querySelector("meta[name='csrf-token']").getAttribute("content"));
        f.append(csrfInp);

        document.querySelector(".modal-vc.modal-vc-slider .vc-dropzone").append(f);

        dropzoneSlider = new Dropzone('.modal-vc-slider .dropzone', {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            // maxFiles: 1,
            addRemoveLinks: true,
            previewTemplate: document.querySelector('.modal-vc-slider #template-preview').innerHTML,
            init: function () {
                // this.on("maxfilesexceeded", function (file) {
                //     this.removeAllFiles();
                //     this.addFile(file);
                // });
            },
            removedfile: function (file) {
                var name = file.serverName;
                fetch("{{route("vc.dropzone.delete")}}", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: name,
                        _token: document.querySelector("meta[name='csrf-token']").getAttribute("content")
                    })
                }).then(res => {

                });
                let modal=document.querySelector(".modal-vc-slider");
                let callerId = modal.getAttribute("data-caller-id");
                let data = document.querySelector(".component-item[data-id='" + callerId + "'] .item-info input[name='component-data']").getAttribute("value");
                data=JSON.parse(data);
                data.totalImage=data.totalImage.filter(item=>item.serverName!=name);

                document.querySelector(".component-item[data-id='" + callerId + "'] .item-info input[name='component-data']").setAttribute("value",JSON.stringify(data));
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

            },
            success: function (file, response) {
                file.serverName = response.data.file_name;
            }
        });
    }


    function getButtonModalData(modal) {
        let btnText = modal.querySelector("input[name='btn-text']").value;
        let btnLink = modal.querySelector("input[name='btn-link']").value;
        let btnType = modal.querySelector("select[name='btn-type']").value;
        let btnTarget = modal.querySelector("select[name='btn-target']").value;
        return {
            text: btnText,
            link: btnLink,
            type: btnType,
            target: btnTarget
        };
    }

    function setButtonModalData(data) {
        let modal = document.querySelector(".modal-vc.modal-vc-button");
        modal.querySelector("input[name='btn-text']").value = data.text ? data.text : "";
        modal.querySelector("input[name='btn-link']").value = data.link ? data.link : "";
        modal.querySelector("select[name='btn-type']").value = data.type ? data.type : "primary";
        modal.querySelector("select[name='btn-target']").value = data.target ? data.target : "_blank";
    }

    function getTextModalData(modal) {
        let mainText = tinymce.get("vc-text-editor").getContent();
        mainText = mainText.replace('"', '\"');

        return {
            html: mainText
        }
    }

    function setTextModalData(data) {
        if(tinymce.get("vc-text-editor").getBody()){
            tinymce.get("vc-text-editor").getBody().innerHTML = data.html && Object.keys(data).length!=0 ? data.html : "<p></p>";
        }
    }

    function getImageModalData(modal) {

        let imgWidth = modal.querySelector("input[name='img-width']").value;
        let imgHeight = modal.querySelector("input[name='img-height']").value;
        let imgWidthType = modal.querySelector("select[name='img-width-type']").value;
        let imgHeightType = modal.querySelector("select[name='img-height-type']").value;
        let imgAlt = modal.querySelector("input[name='img-alt']").value;
        let callerId = modal.getAttribute("data-caller-id");
        let data = document.querySelector(".component-item[data-id='" + callerId + "'] .item-info input[name='component-data']").getAttribute("value");
        let img = JSON.parse(data);
        if (dropzoneImage.getAcceptedFiles().length > 0) {
            let file = dropzoneImage.getAcceptedFiles()[0];
            img["src"] = "{{config("visual-composer.read_dir")}}" + file.serverName;
            img["size"] = file.size;
            img["name"] = file.name;
            img["serverName"] = file.serverName;
        }

        if(imgWidthType=="auto"){
            imgWidth="auto";
        }else{
            imgWidth+=imgWidthType;
        }

        if(imgHeightType=="auto"){
            imgHeight="auto";
        }else{
            imgHeight+=imgHeightType;
        }
        return {
            width: imgWidth,
            height: imgHeight,
            alt: imgAlt,
            src: img.src ? img.src : "",
            name: img.name ? img.name : "",
            serverName: img.serverName ? img.serverName : "",
            size: img.size ? img.size : 0,
        };
    }

    function setImageModalData(data) {
        let modal = document.querySelector(".modal-vc.modal-vc-image");
        let width=data.width ? data.width : "";
        let height= data.height ? data.height : "";
        let widthType="px";
        let heightType="px";

        if(width.includes("px")){
            widthType="px";
            width=width.split("px")[0];
        }else if(width.includes("%")){
            widthType="%";
            width=width.split("%")[0];
        }else{
            widthType="auto";
            width="";
        }

        if(height.includes("px")){
            heightType="px";
            height=height.split("px")[0];
        }else if(height.includes("%")){
            heightType="%";
            height=height.split("%")[0];
        }else{
            heightType="auto";
            height="";
        }
        modal.querySelector("select[name='img-width-type']").value=widthType;
        modal.querySelector("select[name='img-height-type']").value=heightType;
        modal.querySelector("input[name='img-width']").value = width;
        modal.querySelector("input[name='img-height']").value = height;
        modal.querySelector("input[name='img-alt']").value = data.alt ? data.alt : "";


        if (data.src && data.src != "") {
            let mockFile = {
                name: (data.name ? data.name : ""),
                serverName: (data.serverName ? data.serverName : ""),
                size: (data.size ? data.size : 0)
            };
            dropzoneImage.options.addedfile.call(dropzoneImage, mockFile);
            dropzoneImage.options.thumbnail.call(dropzoneImage, mockFile, "{{config("visual-composer.read_dir")}}" + data.src ? data.src : "");
        }
    }

    function getVideoModalData(modal) {
        let videoType = "";
        let radios = modal.querySelectorAll("input[name='video-type']");
        let videoWidth = modal.querySelector("input[name='video-width']").value;
        let videoHeight = modal.querySelector("input[name='video-height']").value;
        for (let i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                videoType = radios[i].value;
                break;
            }
        }
        let callerId = modal.getAttribute("data-caller-id");
        let data = document.querySelector(".component-item[data-id='" + callerId + "'] .item-info input[name='component-data']").getAttribute("value");

        let video = JSON.parse(data);
        switch (videoType) {
            case "upload":
                if (dropzoneVideo.getAcceptedFiles().length > 0) {
                    let file = dropzoneVideo.getAcceptedFiles()[0];
                    video["src"] = "{{config("visual-composer.read_dir")}}" + file.serverName;
                    video["size"] = file.size;
                    video["name"] = file.name;
                    video["serverName"] = file.serverName;
                }
                modal.querySelector("input[name='video-url']").value = "";
                break;
            case "url":
                let videoUrl = modal.querySelector("input[name='video-url']").value;
                video["src"] = videoUrl;
                video["size"] = "";
                video["name"] = "";
                video["serverName"] = "";

                dropzoneVideo.removeAllFiles();
                break;
        }

        return {
            type: videoType,
            src: video.src ? video.src : "",
            name: video.name ? video.name : "",
            serverName: video.serverName ? video.serverName : "",
            size: video.size ? video.size : 0,
            width: videoWidth,
            height: videoHeight,

        };
    }

    function setVideoModalData(data) {
        let modal = document.querySelector(".modal-vc.modal-vc-video");
        let videoType = data.type ? data.type : "upload";
        modal.querySelector("input[name='video-type'][value='" + videoType + "']").checked = true;
        modal.querySelector("input[name='video-width']").value = data.width ? data.width : "";
        modal.querySelector("input[name='video-height']").value = data.height ? data.height : "";

        if (data.src && data.src != "" && data.type == "upload") {
            let mockFile = {
                name: (data.name ? data.name : ""),
                serverName: (data.serverName ? data.serverName : ""),
                size: (data.size ? data.size : 0)
            };
            dropzoneVideo.options.addedfile.call(dropzoneVideo, mockFile);
            dropzoneVideo.options.thumbnail.call(dropzoneVideo, mockFile, "{{config("visual-composer.read_dir")}}" + data.src ? data.src : "");
        }

    }

    function getEmptyModalData(modal) {
        let height = modal.querySelector("input[name='empty-height']").value;
        return {
            height: height,
        };
    }

    function setEmptyModalData(data) {
        let modal = document.querySelector(".modal-vc.modal-vc-empty");
        modal.querySelector("input[name='empty-height']").value = data.height ? data.height : "";
    }

    function getWarningModalData(modal) {
        let type = modal.querySelector("select[name='warning-type']").value;
        let text = modal.querySelector("textarea[name='vc-warning']").value;
        return {
            type: type,
            text : text
        }
    }

    function setWarningModalData(data) {
        let modal = document.querySelector(".modal-vc.modal-vc-warning");
        modal.querySelector("select[name='warning-type']").value = data.type ? data.type : "warning";
        modal.querySelector("textarea[name='vc-warning']").value = data.text ? data.text :"";
    }

    function getCodeModalData(modal) {
        let code = modal.querySelector("textarea[name='vc-code']").value;
        return {
            code: code,
        }
    }

    function setCodeModalData(data) {
        let modal = document.querySelector(".modal-vc.modal-vc-code");
        modal.querySelector("textarea[name='vc-code']").value = data.code ? data.code :"";
    }

    function getSliderModalData(modal) {
        let callerId = modal.getAttribute("data-caller-id");
        let data = document.querySelector(".component-item[data-id='" + callerId + "'] .item-info input[name='component-data']").getAttribute("value");
        data = JSON.parse(data);
        let total = [];
        if("totalImage" in data && data.totalImage.length>0){
            total=data.totalImage;
        }
        let dropSlider = dropzoneSlider.getAcceptedFiles();
        for(i=0; i < dropSlider.length; i++) {
            let file = dropSlider[i];
            total.push({
                "src": "{{config("visual-composer.read_dir")}}" + file.serverName,
                "size": file.size,
                "name": file.name,
                "serverName": file.serverName,
                'title':file.previewElement.querySelector('input[name="title"]').value,
                'description':file.previewElement.querySelector('textarea[name="description"]').value,
            });
        }
        return {
            totalImage: total,
        }


    }

    function setSliderModalData(data) {
        if(Object.keys(data).length > 0) {
            let dataImage=data['totalImage'];
            for(i=0; i < dataImage.length; i++) {
                let mockFile = {
                    name: (dataImage[i]['name'] ? dataImage[i]['name'] : ""),
                    serverName: (dataImage[i]['serverName'] ? dataImage[i]['serverName'] : ""),
                    size: (dataImage[i]['size'] ? dataImage[i]['size'] : 0),
                };

                dropzoneSlider.options.addedfile.call(dropzoneSlider, mockFile);
                dropzoneSlider.options.thumbnail.call(dropzoneSlider, mockFile, "{{config("visual-composer.read_dir")}}" + dataImage[i]['src'] ? dataImage[i]['src'] : "");
                mockFile.previewElement.querySelector('input[name="title"]').value=dataImage[i]['title'];
                mockFile.previewElement.querySelector('textarea[name="description"]').value=dataImage[i]['description'];
                mockFile.previewElement.querySelector('.dz-progress').remove();
            }

        }
    }

</script>
