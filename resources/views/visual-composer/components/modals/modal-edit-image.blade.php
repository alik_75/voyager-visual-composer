<div class="modal-vc modal-vc-image" onmousedown="modalOnMouseDown(this)" onmouseup="modalOnMouseUp(this)">
    <div class="modal-dialog-vc" onmouseup="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::image.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">
            <div class="vc-form-group vc-dropzone">
                <label>{{ __('visualcomposer::image.image') }}</label>
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::image.alt') }}</label>
                <input class="vc-input" name="img-alt" placeholder="{{ __('visualcomposer::image.alt') }}" />
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::image.width') }}</label>
                <div class="row">
                    <div class="col-md-8">
                        <input class="vc-input" name="img-width"
                            placeholder="{{ __('visualcomposer::image.width') }}" />
                    </div>
                    <div class="col-md-4">
                        <select onchange="widthHeightTypeChange(this)" class="vc-input" name="img-width-type">
                            <option value="%">%</option>
                            <option value="px">px</option>
                            <option value="auto">auto</option>

                        </select>
                    </div>
                </div>

            </div>
            <div class="vc-form-group">

                <label>{{ __('visualcomposer::image.height') }}</label>
                <div class="row">
                    <div class="col-md-8">
                        <input class="vc-input" name="img-height"
                            placeholder="{{ __('visualcomposer::image.height') }}" />
                    </div>
                    <div class="col-md-4">
                        <select onchange="widthHeightTypeChange(this)" class="vc-input" name="img-height-type">
                            <option value="%">%</option>
                            <option value="px">px</option>
                            <option value="auto">auto</option>

                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
