<div class="modal-vc modal-vc-video" onclick="onCloseVCModal(this);">
    <div class="modal-dialog-vc" onclick="modalClick(event)">
        <div class="modal-header-vc">
            <button type="button" onclick="onCloseVCModal(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::video.component_name') }}</strong>
        </div>
        <div class="modal-body-vc">

            <div class="vc-form-group video-type-selector">
                <label>{{ __('visualcomposer::video.video_type') }}</label>
                <br>
                <input type="radio" id="upload" name="video-type" onclick="videoTypeChanged(this)" checked
                    value="upload">
                <label for="upload">{{ __('visualcomposer::video.upload') }}</label>
                <input type="radio" id="url" name="video-type" onclick="videoTypeChanged(this)" value="url">
                <label for="url">{{ __('visualcomposer::video.url') }}</label>

                <div class="type-content">
                    <div class="vc-dropzone"></div>
                    <input class="vc-input" style="display: none" name="video-url"
                        placeholder="{{ __('visualcomposer::video.url') }}">
                </div>
            </div>
            <div class="vc-form-group">
                <label>{{ __('visualcomposer::video.width_label') }}</label>
                <input class="vc-input" name="video-width"
                    placeholder="{{ __('visualcomposer::video.width_placeholder') }}" />
            </div>
            <div class="vc-form-group">

                <label>{{ __('visualcomposer::video.height_label') }}</label>
                <input class="vc-input" name="video-height"
                    placeholder="{{ __('visualcomposer::video.height_placeholder') }}" />
            </div>
        </div>
        <div class="modal-footer-vc">
            <button type="button" class="vc-btn vc-btn-submit"
                onclick="onModalSubmit(this)">{{ __('visualcomposer::general.modal_save') }}</button>
            <button type="button" class="vc-btn vc-btn-close"
                onclick="onCloseVCModal(this)">{{ __('visualcomposer::general.modal_cancel') }}</button>
        </div>
    </div>
</div>
