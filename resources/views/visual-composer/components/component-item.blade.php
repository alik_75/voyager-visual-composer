<div class="component-item list-item" @if (Lang::locale() == 'fa') dir="rtl" @else dir="ltr" @endif>
    <div class="item-info">
        <input type="hidden" onchange="ChangeComponentData(this)" name="component-data" value="{}">
        <span>Text Editor</span>
        <small>Custom Text</small>
    </div>
    <div class="item-icon">
        <img width="100%" src="/storage/users/default.png" />
    </div>
    <div class="item-actions">
        <div class="btn btn-default btn-action btn-delete" onclick="deleteComponent(this)"><i class="vc-icon">B</i>
        </div>
        <div class="btn btn-default btn-action btn-copy" onclick="onCopyComponent(this)"><i class="vc-icon">E</i> </div>
        <div class="btn btn-default btn-action btn-edit" onclick="onOpenVCModal(this)"><i class="vc-icon">C</i> </div>
    </div>
</div>
