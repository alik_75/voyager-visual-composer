<div class="vc-component-selector" onclick="closeModalVC(this);">

    <div class="vc-modal" onclick="modalClick(event)">
        <div class="vc-modal-header">
            <button type="button" onclick="closeModalVC(this);" class="close" data-dismiss="modal">&times;</button>
            <strong>{{ __('visualcomposer::general.choose_your_component') }}</strong>
        </div>
        <div class="vc-modal-body">
            <ul class="vc-component-selector-list">

            </ul>
        </div>
        <div class="vc-modal-footer">

        </div>
    </div>
</div>

<script>
    let componentLists = [{
            "type": "text",
            "title": "{{ __('visualcomposer::general.text_editor') }}",
            "sub_title": "{{ __('visualcomposer::general.text_editor_desc') }}",
            "icon": "/storage/users/default.png"
        },
        {
            "type": "button",
            "title": "{{ __('visualcomposer::general.button') }}",
            "sub_title": "{{ __('visualcomposer::general.button_desc') }}",
            "icon": "/storage/users/default.png"
        },
        {
            "type": "image",
            "title": "{{ __('visualcomposer::general.image') }}",
            "sub_title": "{{ __('visualcomposer::general.image_desc') }}",
            "icon": "/storage/users/default.png"
        },
        {
            "type": "video",
            "title": "{{ __('visualcomposer::general.video') }}",
            "sub_title": "{{ __('visualcomposer::general.video_desc') }}",
            "icon": "/storage/users/default.png"
        },
        {
            "type": "empty",
            "title": "{{ __('visualcomposer::general.empty_space') }}",
            "sub_title": "{{ __('visualcomposer::general.empty_space_desc') }}",
            "icon": "/storage/users/default.png"
        },
        {
            "type": "warning",
            "title": "{{ __('visualcomposer::general.info_alert') }}",
            "sub_title": "{{ __('visualcomposer::general.info_alert_desc') }}",
            "icon": "/storage/users/default.png"
        },
        {
            "type": "code",
            "title": "{{ __('visualcomposer::general.code') }}",
            "sub_title": "{{ __('visualcomposer::general.code_desc') }}",
            "icon": "/storage/users/default.png"
        },
        {
            "type": "slider",
            "title": "{{ __('visualcomposer::general.slider') }}",
            "sub_title": "{{ __('visualcomposer::general.slider_desc') }}",
            "icon": "/storage/users/default.png"
        }
    ];
    initialListComponents();

    function closeModalVC(elem) {
        elem.closest(".vc-component-selector").classList.remove("open");
        document.body.classList.remove("open-modal");
    }

    function modalClick(e) {
        e.stopPropagation();
    }

    function openComponentSelector(elem) {
        let colId = elem.closest(".vc-column").getAttribute("data-id");
        let rowId = elem.closest(".vc-row").getAttribute("data-id");
        document.querySelector(".vc-component-selector").setAttribute("data-caller-id", rowId + "-" + colId);
        document.querySelector(".vc-component-selector").classList.add("open");
        document.body.classList.add("open-modal");
    }

    function componentClick(elem) {
        elem = elem.currentTarget;
        let rowColId = elem.closest(".vc-component-selector").getAttribute("data-caller-id").split("-");
        let componentType = elem.querySelector(".component-item").getAttribute("data-type");
        let component = createComponentByType(componentType);
        let componentContainer = document.querySelector(".vc-container .vc-row[data-id='" + rowColId[0] +
            "'] .vc-column[data-id='" + rowColId[1] + "'] .vc-column-container");
        if (componentContainer.querySelector(".column-empty-message")) {
            componentContainer.querySelector(".column-empty-message").remove();
            componentContainer.style.display = "block";
        }
        component.setAttribute("data-id", rowColId.join("_") + "_" + (componentContainer.childElementCount + 1));
        componentContainer.append(component);
        initDragableComponents();
        closeModalVC(elem);
        saveInfoVC();
    }

    function initialListComponents() {
        let elem = document.querySelector(".vc-component-selector .vc-component-selector-list");


        for (let i = 0; i < componentLists.length; i++) {
            let liItem = document.createElement("li");
            liItem.addEventListener("click", componentClick, false);
            let component = createComponentByType(componentLists[i].type);
            component.classList.add("selector-item");
            liItem.append(component);
            elem.append(liItem);
        }
    }

    function createComponentByType(type) {
        let SVGs = {
            text: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 17646.2 17560.13">\n' +
                ' <defs>\n' +
                '  <style type="text/css">\n' +
                '   <![CDATA[\n' +
                '    .fil0 {fill:black}\n' +
                '   ]]>\n' +
                '  </style>\n' +
                ' </defs>\n' +
                ' <g id="Layer_x0020_1">\n' +
                '  <metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '  <path class="fil0" d="M17646.2 3475.78l0 10608.58c0,1914.36 -1561.42,3475.78 -3475.78,3475.78l-10694.65 0c-1914.36,0 -3475.78,-1561.42 -3475.78,-3475.78l0 -10608.58c0,-1914.36 1561.42,-3475.78 3475.78,-3475.78l10694.65 0c1914.36,0 3475.78,1561.42 3475.78,3475.78zm-13286.6 4253.58c-222.07,0 -402.18,-180.12 -402.18,-402.18 0,-222.07 180.12,-402.18 402.18,-402.18l8927.01 0c222.07,0 402.18,180.12 402.18,402.18 0,222.07 -180.12,402.18 -402.18,402.18l-8927.01 0zm1958.81 -2341.84c-222.07,0 -402.18,-180.12 -402.18,-402.18 0,-222.07 180.12,-402.18 402.18,-402.18l5009.38 0c222.07,0 402.18,180.12 402.18,402.18 0,222.07 -180.12,402.18 -402.18,402.18l-5009.38 0zm-1958.81 4091.02c-222.07,0 -402.18,-180.12 -402.18,-402.18 0,-222.07 180.12,-402.18 402.18,-402.18l8927.01 0c222.07,0 402.18,180.12 402.18,402.18 0,222.07 -180.12,402.18 -402.18,402.18l-8927.01 0zm0 1749.17c-222.07,0 -402.18,-180.12 -402.18,-402.18 0,-222.07 180.12,-402.18 402.18,-402.18l8927.01 0c222.07,0 402.18,180.12 402.18,402.18 0,222.07 -180.12,402.18 -402.18,402.18l-8927.01 0zm0 1749.17c-222.07,0 -402.18,-180.12 -402.18,-402.18 0,-222.07 180.12,-402.18 402.18,-402.18l8927.01 0c222.07,0 402.18,180.12 402.18,402.18 0,222.07 -180.12,402.18 -402.18,402.18l-8927.01 0zm12080.17 1107.48l0 -10608.58c0,-624.02 -255.13,-1191.27 -666.66,-1602.69 -411.42,-411.53 -978.67,-666.66 -1602.69,-666.66l-10694.65 0c-624.02,0 -1191.38,255.24 -1602.69,666.66 -411.53,411.42 -666.66,978.67 -666.66,1602.69l0 10608.58c0,624.02 255.24,1191.38 666.66,1602.69 411.3,411.42 978.67,666.66 1602.69,666.66l10694.65 0c624.02,0 1191.27,-255.13 1602.69,-666.66 411.42,-411.3 666.66,-978.67 666.66,-1602.69z"/>\n' +
                ' </g>\n' +
                '</svg>',
            button: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 60010.55 59717.85">\n' +
                ' <defs>\n' +
                '  <style type="text/css">\n' +
                '   <![CDATA[\n' +
                '    .fil0 {fill:black}\n' +
                '   ]]>\n' +
                '  </style>\n' +
                ' </defs>\n' +
                ' <g id="Layer_x0020_1">\n' +
                '  <metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '  <path class="fil0" d="M60010.55 11820.29l0 36077.27c0,6510.27 -5310.02,11820.29 -11820.29,11820.29l-36369.97 0c-6510.27,0 -11820.29,-5310.02 -11820.29,-11820.29l0 -36077.27c0,-6510.27 5310.02,-11820.29 11820.29,-11820.29l36369.97 0c6510.27,0 11820.29,5310.02 11820.29,11820.29zm-29759.68 13344.64l-11731.9 6453.67c-324.49,178.33 -512.12,495.84 -512.12,866.07 0,370.23 187.64,687.74 512.12,866.07l11731.9 6453.67c314.79,173.29 670.29,167.86 979.66,-15.12 309.37,-182.98 485.37,-491.58 485.37,-850.95l0 -4402.08 7329.43 0c1629.02,0 2957.98,-1328.96 2957.98,-2957.98l0 -10802.25c0,-544.3 -444.28,-988.58 -988.58,-988.58l-2126.02 0c-544.3,0 -988.58,444.28 -988.58,988.58l0 8668.47c0,544.3 -444.28,988.58 -988.58,988.58l-5195.65 0 0 -4402.08c0,-359.38 -176.01,-667.97 -485.37,-850.95 -309.37,-182.98 -664.87,-188.41 -979.66,-15.12zm25656.89 22732.64l0 -36077.27c0,-2122.15 -867.62,-4051.23 -2267.14,-5450.36 -1399.13,-1399.52 -3328.21,-2267.14 -5450.36,-2267.14l-36369.97 0c-2122.15,0 -4051.62,868.01 -5450.36,2267.14 -1399.52,1399.13 -2267.14,3328.21 -2267.14,5450.36l0 36077.27c0,2122.15 868.01,4051.62 2267.14,5450.36 1398.74,1399.13 3328.21,2267.14 5450.36,2267.14l36369.97 0c2122.15,0 4051.23,-867.62 5450.36,-2267.14 1399.13,-1398.74 2267.14,-3328.21 2267.14,-5450.36z"/>\n' +
                ' </g>\n' +
                '</svg>',
            image: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 60010.55 59717.85">\n' +
                ' <defs>\n' +
                '  <style type="text/css">\n' +
                '   <![CDATA[\n' +
                '    .fil0 {fill:black}\n' +
                '   ]]>\n' +
                '  </style>\n' +
                ' </defs>\n' +
                ' <g id="Layer_x0020_1">\n' +
                '  <metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '  <path class="fil0" d="M60010.55 11820.29l0 36077.27c0,6510.27 -5310.02,11820.29 -11820.29,11820.29l-36369.97 0c-6510.27,0 -11820.29,-5310.02 -11820.29,-11820.29l0 -36077.27c0,-6510.27 5310.02,-11820.29 11820.29,-11820.29l36369.97 0c6510.27,0 11820.29,5310.02 11820.29,11820.29zm-55907.76 22746.59l14702.28 -14702.28 4629.65 -4629.65 32473.04 32473.42 0 -35888.09c0,-2122.15 -867.62,-4051.23 -2267.14,-5450.36 -1399.13,-1399.52 -3328.21,-2267.14 -5450.36,-2267.14l-36369.97 0c-2122.15,0 -4051.62,868.01 -5450.36,2267.14 -1399.52,1399.13 -2267.14,3328.21 -2267.14,5450.36l0 22746.59z"/>\n' +
                ' </g>\n' +
                '</svg>',
            video: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.571248in" height="0.400008in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 15504.37 10856.7">\n' +
                ' <defs>\n' +
                '  <style type="text/css">\n' +
                '   <![CDATA[\n' +
                '    .fil0 {fill:black}\n' +
                '   ]]>\n' +
                '  </style>\n' +
                ' </defs>\n' +
                ' <g id="Layer_x0020_1">\n' +
                '  <metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '  <path class="fil0" d="M766.37 4904.44l9197.14 0c421.44,0 766.26,345.14 766.26,766.37l0 4419.64c0,421.01 -345.25,766.26 -766.26,766.26l-9197.14 0c-421.12,0 -766.37,-344.93 -766.37,-766.26l0 -4419.64c0,-421.65 344.93,-766.37 766.37,-766.37zm2028.11 -4904.44c1069.94,0 1937.29,867.34 1937.29,1937.29 0,1070.05 -867.34,1937.29 -1937.29,1937.29 -1069.94,0 -1937.29,-867.24 -1937.29,-1937.29 0,-1069.94 867.34,-1937.29 1937.29,-1937.29zm0 918.95c562.38,0 1018.33,455.95 1018.33,1018.33 0,562.49 -455.95,1018.33 -1018.33,1018.33 -562.38,0 -1018.33,-455.84 -1018.33,-1018.33 0,-562.38 455.95,-1018.33 1018.33,-1018.33zm5140.91 -918.95c1070.05,0 1937.29,867.34 1937.29,1937.29 0,1070.05 -867.24,1937.29 -1937.29,1937.29 -1069.94,0 -1937.29,-867.24 -1937.29,-1937.29 0,-1069.94 867.34,-1937.29 1937.29,-1937.29zm0 918.95c562.49,0 1018.33,455.95 1018.33,1018.33 0,562.49 -455.84,1018.33 -1018.33,1018.33 -562.38,0 -1018.22,-455.84 -1018.22,-1018.33 0,-562.38 455.84,-1018.33 1018.22,-1018.33zm4710.39 5368.19l2092.23 -1268.8c360.53,-218.63 766.37,93.18 766.37,512.69l0 4699.49c0,419.19 -407.97,730.03 -766.37,512.69l-2092.23 -1268.69c-358.29,-217.45 -766.37,-344.93 -766.37,-766.58l0 -1654.55c0,-421.54 405.73,-547.63 766.37,-766.26z"/>\n' +
                ' </g>\n' +
                '</svg>',
            empty: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 60010.55 59717.85">\n' +
                ' <defs>\n' +
                '  <style type="text/css">\n' +
                '   <![CDATA[\n' +
                '    .fil0 {fill:black}\n' +
                '   ]]>\n' +
                '  </style>\n' +
                ' </defs>\n' +
                ' <g id="Layer_x0020_1">\n' +
                '  <metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '  <path class="fil0" d="M60010.55 11820.29l0 36077.27c0,6510.27 -5310.02,11820.29 -11820.29,11820.29l-36369.97 0c-6510.27,0 -11820.29,-5310.02 -11820.29,-11820.29l0 -36077.27c0,-6510.27 5310.02,-11820.29 11820.29,-11820.29l36369.97 0c6510.27,0 11820.29,5310.02 11820.29,11820.29zm-31373.19 24362.05l0 10294 -4103.18 -109.33 5470.91 5470.91 5470.91 -5470.91 -4103.18 109.33 0 -10294 -2735.45 0zm2735.45 -12646.82l0 -10294 4103.18 109.33 -5470.91 -6947.96 -5470.91 6947.96 4103.18 -109.33 0 10294 2735.45 0zm24534.95 24362.05l0 -36077.27c0,-2122.15 -867.62,-4051.23 -2267.14,-5450.36 -1399.13,-1399.52 -3328.21,-2267.14 -5450.36,-2267.14l-36369.97 0c-2122.15,0 -4051.62,868.01 -5450.36,2267.14 -1399.52,1399.13 -2267.14,3328.21 -2267.14,5450.36l0 36077.27c0,2122.15 868.01,4051.62 2267.14,5450.36 1398.74,1399.13 3328.21,2267.14 5450.36,2267.14l36369.97 0c2122.15,0 4051.23,-867.62 5450.36,-2267.14 1399.13,-1398.74 2267.14,-3328.21 2267.14,-5450.36z"/>\n' +
                ' </g>\n' +
                '</svg>',
            warning: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 254.13 252.89">\n' +
                '<g id="Layer_x0020_1">\n' +
                '<metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '<path fill="black" d="M254.13 50.06l0 152.78c0,27.57 -22.49,50.06 -50.06,50.06l-154.02 0c-27.57,0 -50.06,-22.49 -50.06,-50.06l0 -152.78c0,-27.57 22.49,-50.06 50.06,-50.06l154.02 0c27.57,0 50.06,22.49 50.06,50.06zm-138.4 117.87l22.67 0 0 -124.57 -22.67 0 0 124.57zm11.45 41.6c8.41,0 14.02,-5.61 14.02,-14.26 0,-8.41 -5.61,-14.02 -14.02,-14.02 -8.65,0 -14.26,5.61 -14.26,14.02 0,8.65 5.61,14.26 14.26,14.26zm109.57 -6.7l0 -152.78c0,-17.97 -14.71,-32.68 -32.68,-32.68l-154.02 0c-17.97,0 -32.68,14.71 -32.68,32.68l0 152.78c0,17.97 14.71,32.68 32.68,32.68l154.02 0c17.97,0 32.68,-14.71 32.68,-32.68z"/>\n' +
                '</g>\n' +
                '</svg>',
            code: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 129.34 128.71">\n' +
                '<g id="Layer_x0020_1">\n' +
                '<metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '<path fill="black" d="M129.34 25.48l0 77.76c0,14.03 -11.44,25.48 -25.48,25.48l-78.39 0c-14.03,0 -25.48,-11.44 -25.48,-25.48l0 -77.76c0,-14.03 11.44,-25.48 25.48,-25.48l78.39 0c14.03,0 25.48,11.44 25.48,25.48zm-102.53 39.1l16.4 9.45 0 9.72 -27.03 -15.83 0 -6.73 27.03 -15.83 0 9.71 -16.4 9.5zm52.8 -18.18l-18.52 33.96 -11.38 0 18.71 -33.96 11.19 0zm22.91 18.18l-16.4 -9.5 0 -9.71 27.03 15.83 0 6.73 -27.03 15.83 0 -9.72 16.4 -9.45zm17.97 38.66l0 -77.76c0,-9.15 -7.48,-16.63 -16.63,-16.63l-78.39 0c-9.15,0 -16.63,7.48 -16.63,16.63l0 77.76c0,9.15 7.48,16.63 16.63,16.63l78.39 0c9.15,0 16.63,-7.48 16.63,-16.63z"/>\n' +
                '</g>\n' +
                '</svg>',
            slider: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 60010.55 59717.85">\n' +
                '<g id="Layer_x0020_1">\n' +
                '<metadata id="CorelCorpID_0Corel-Layer"/>\n' +
                '<path fill="black" d="M60010.55 11820.29l0 36077.27c0,6510.27 -5310.02,11820.29 -11820.29,11820.29l-36369.97 0c-6510.27,0 -11820.29,-5310.02 -11820.29,-11820.29l0 -36077.27c0,-6510.27 5310.02,-11820.29 11820.29,-11820.29l36369.97 0c6510.27,0 11820.29,5310.02 11820.29,11820.29zm-43819.2 29123.11c2582.32,0 4675.39,2093.07 4675.39,4675.39 0,2582.32 -2093.07,4675.39 -4675.39,4675.39 -2582.32,0 -4675.39,-2093.07 -4675.39,-4675.39 0,-2582.32 2093.07,-4675.39 4675.39,-4675.39zm13813.73 0c2582.32,0 4675.39,2093.07 4675.39,4675.39 0,2582.32 -2093.07,4675.39 -4675.39,4675.39 -2582.32,0 -4675.39,-2093.07 -4675.39,-4675.39 0,-2582.32 2093.07,-4675.39 4675.39,-4675.39zm13813.73 0c2582.32,0 4675.39,2093.07 4675.39,4675.39 0,2582.32 -2093.07,4675.39 -4675.39,4675.39 -2582.32,0 -4675.39,-2093.07 -4675.39,-4675.39 0,-2582.32 2093.07,-4675.39 4675.39,-4675.39zm-169.42 -25630.14l0 16404.96c0,2960.69 -2414.46,5375.15 -5375.15,5375.15l-16538.32 0c-2960.31,0 -5374.76,-2414.46 -5374.76,-5375.15l0 -16404.96c0,-2960.69 2414.46,-5375.15 5374.76,-5375.15l16538.32 0c2960.69,0 5375.15,2414.46 5375.15,5375.15zm-25422.73 10343.24l8790.59 -8790.59 14766.64 14766.25 0 -16318.9c0,-1930.25 -1579.4,-3509.65 -3509.65,-3509.65l-16538.32 0c-1930.25,0 -3509.26,1579.4 -3509.26,3509.65l0 10343.24zm37681.1 22241.06l0 -36077.27c0,-4244.68 -3472.82,-7717.5 -7717.5,-7717.5l-36369.97 0c-4244.68,0 -7717.5,3472.82 -7717.5,7717.5l0 36077.27c0,4244.68 3472.82,7717.5 7717.5,7717.5l36369.97 0c4244.68,0 7717.5,-3472.82 7717.5,-7717.5z"/>\n' +
                '</g>\n' +
                '</svg>'
        }
        let componentTemplate = document.querySelector(".vc-components .component-item").cloneNode(true);
        componentLists.forEach(item => {
            if (item.type == type) {
                componentTemplate.querySelector(".item-info span").innerHTML = item.title;
                componentTemplate.querySelector(".item-info small").innerHTML = item.sub_title;
                componentTemplate.querySelector(".item-icon").innerHTML = SVGs[item.type];
                componentTemplate.setAttribute("data-type", item.type);

            }
        });
        return componentTemplate;
    }
</script>
