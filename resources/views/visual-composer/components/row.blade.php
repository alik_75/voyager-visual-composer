<div class="vc-row" data-structure="12" @if (Lang::locale() == 'fa') dir="rtl" @else dir="ltr" @endif>
    <input type="hidden" name="row-data" value="{}">
    <div class="vc-row-options">
        <div class="right">
            <button type="button" onclick="openRowInfoModal(this)"><i class="vc-icon">C</i></button>
            <button type="button" onclick="removeRowVC(this)"><i class="vc-icon">B</i></button>

        </div>
        <div class="left">
            <button type="button" class="btn-template-select"> <span> <svg xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003"
                        xml:space="preserve" width="0.609429in" height="0.606457in" version="1.1"
                        style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                        viewBox="0 0 68.65 68.31">
                        <defs>
                            <style type="text/css">
                                < ! [CDATA[ .fil0 {
                                    fill: black;
                                    fill-rule: nonzero
                                }

                                ]]>
                            </style>
                        </defs>
                        <g id="Layer_x0020_1">
                            <metadata id="CorelCorpID_0Corel-Layer" />
                            <polygon class="fil0" points="68.65,0 68.65,68.31 -0,68.31 -0,0 " />
                        </g>
                    </svg></span>
                <ul class="template-selector">

                    <li onclick="colStructureClick(this)" class="active" data-structure="12">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in"
                            height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 68.65 68.31">
                            <defs>
                                <style type="text/css">
                                    < ! [CDATA[ .fil0 {
                                        fill: black;
                                        fill-rule: nonzero
                                    }

                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <polygon class="fil0" points="68.65,0 68.65,68.31 -0,68.31 -0,0 " />
                            </g>
                        </svg>
                    </li>
                    <li onclick="colStructureClick(this)" data-structure="6-6"><svg xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in"
                            height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 68.65 68.31">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <g id="_1828001980560">
                                    <polygon class="fil0" points="30.61,0 30.61,68.31 -0,68.31 -0,0 " />
                                    <polygon class="fil0" points="68.65,0 68.65,68.31 38.04,68.31 38.04,0 " />
                                </g>
                            </g>
                        </svg></li>

                    <li onclick="colStructureClick(this)" data-structure="8-4"><svg xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609429in"
                            height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 94.82 94.35">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <polygon class="fil0" points="32.22,0 32.22,94.35 -0,94.35 -0,0 " />
                                <polygon class="fil0" points="94.82,0 94.82,94.35 42.28,94.35 42.28,0 " />
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="4-4-4"><svg xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve" width="0.609425in"
                            height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 68.65 68.31">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <g id="_1828002031808">
                                    <g>
                                        <polygon class="fil0" points="19.75,0 19.75,68.31 -0,68.31 -0,0 " />
                                        <polygon class="fil0" points="44.29,0 44.29,68.31 24.54,68.31 24.54,0 " />
                                    </g>
                                    <polygon class="fil0" points="68.65,0 68.65,68.31 48.9,68.31 48.9,0 " />
                                </g>
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="3-3-3-3"><svg
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve"
                            width="0.609429in" height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 68.65 68.31">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <g id="_1827975070656">
                                    <g>
                                        <polygon class="fil0" points="14.58,0 14.58,68.31 -0,68.31 -0,0 " />
                                        <polygon class="fil0" points="32.71,0 32.71,68.31 18.12,68.31 18.12,0 " />
                                    </g>
                                    <polygon class="fil0" points="50.69,0 50.69,68.31 36.11,68.31 36.11,0 " />
                                </g>
                                <polygon class="fil0" points="68.65,0 68.65,68.31 54.06,68.31 54.06,0 " />
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="9-3"><svg
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve"
                            width="0.609429in" height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 94.82 94.35">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <polygon class="fil0" points="62.76,0 62.76,94.35 -0,94.35 -0,0 " />
                                <polygon class="fil0" points="94.82,0 94.82,94.35 70.4,94.35 70.4,0 " />
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="3-6-3"><svg
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve"
                            width="0.609429in" height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 53.8 53.54">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <polygon class="fil0" points="11.43,0 11.43,53.54 -0,53.54 -0,0 " />
                                <polygon class="fil0" points="39.73,0 39.73,53.54 14.2,53.54 14.2,0 " />
                                <polygon class="fil0" points="53.8,0 53.8,53.54 42.37,53.54 42.37,0 " />
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="10-2"><svg
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve"
                            width="0.609429in" height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 94.82 94.35">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <polygon class="fil0" points="77.22,0 77.22,94.35 -0,94.35 -0,0 " />
                                <polygon class="fil0" points="94.82,0 94.82,94.35 83.98,94.35 83.98,0 " />
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="2-2-2-2-2-2"><svg
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve"
                            width="0.609425in" height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 68.65 68.31">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <g id="_1827983188640">
                                    <g>
                                        <polygon class="fil0" points="9.56,0 9.56,68.31 -0,68.31 -0,0 " />
                                        <polygon class="fil0" points="21.43,0 21.43,68.31 11.88,68.31 11.88,0 " />
                                    </g>
                                    <polygon class="fil0" points="33.22,0 33.22,68.31 23.66,68.31 23.66,0 " />
                                </g>
                                <polygon class="fil0" points="44.98,0 44.98,68.31 35.43,68.31 35.43,0 " />
                                <polygon class="fil0" points="56.86,0 56.86,68.31 47.3,68.31 47.3,0 " />
                                <polygon class="fil0" points="68.65,0 68.65,68.31 59.09,68.31 59.09,0 " />
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="2-8-2"><svg
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve"
                            width="0.609429in" height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 94.82 94.35">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <polygon class="fil0" points="16.11,0 16.11,94.35 -0,94.35 -0,0 " />
                                <polygon class="fil0" points="94.82,0 94.82,94.35 78.71,94.35 78.71,0 " />
                                <polygon class="fil0" points="73.39,0 73.39,94.35 20.86,94.35 20.86,0 " />
                            </g>
                        </svg></li>
                    <li onclick="colStructureClick(this)" data-structure="2-2-2-6"><svg
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xmlns:xodm="http://www.corel.com/coreldraw/odm/2003" xml:space="preserve"
                            width="0.609421in" height="0.606457in" version="1.1"
                            style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                            viewBox="0 0 94.81 94.35">
                            <defs>
                                <style type="text/css">
                                    <![CDATA[
                                    .fil0 {
                                        fill: gray;
                                        fill-rule: nonzero
                                    }
                                    ]]>
                                </style>
                            </defs>
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer" />
                                <g id="_1827983241680">
                                    <g>
                                        <polygon class="fil0" points="13.2,0 13.2,94.35 -0,94.35 -0,0 " />
                                        <polygon class="fil0" points="29.6,0 29.6,94.35 16.4,94.35 16.4,0 " />
                                    </g>
                                    <polygon class="fil0" points="45.88,0 45.88,94.35 32.68,94.35 32.68,0 " />
                                </g>
                                <polygon class="fil0" points="94.81,0 94.81,94.35 48.93,94.35 48.93,0 " />
                            </g>
                        </svg></li>
                </ul>
            </button>
            <button type="button" class=""><i class="vc-icon btn-vc-row-dragable">@</i></button>



        </div>

    </div>
    <div class="vc-row-container">
        @include('VisualComposer::visual-composer.components.column')
    </div>

</div>
