<div class="vc-column" data-id="1" style="width: 100%">
    <div class="vc-column-border">
        <div class="vc-column-container" style="display: flex">
            <span class="column-empty-message">{{ __('visualcomposer::general.click_to_add_item') }}</span>
        </div>

        <div class="vc-column-option">
            <div class="right"></div>
            <div class="left">
                <button type="button"><i class="vc-icon btn-vc-column-dragable">@</i></button>
                <button type="button" onclick="openComponentSelector(this)" data-toggle="modal"
                    data-target="#vc-component-selector-modal"><i class="vc-icon">A</i></button>


            </div>

        </div>
    </div>
</div>
