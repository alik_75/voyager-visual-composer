<input type="hidden" class="form-control" name="{{ $row->field }}" data-name="{{ $row->display_name }}"
    @if ($row->required == 1) required @endif step="any"
    placeholder="{{ isset($options->placeholder) ? old($row->field, $options->placeholder) : $row->display_name }}"
    value="@if (isset($dataTypeContent->{$row->field})) {{ old($row->field, $dataTypeContent->{$row->field}) }}@else{{ old($row->field) }} @endif">
<div class="vc-container">
    <div class="vc-rows">

    </div>
    <div class="vc-add-row">
        <button type="button" class="btn btn-sm btn-info" onclick="addRowVC()"><i class="vc-icon">A</i>
            {{ __('visualcomposer::general.row') }}</button>
    </div>
</div>

<div class="vc-components">
    @include('VisualComposer::visual-composer.components.row')
    @include('VisualComposer::visual-composer.components.column')
    @include('VisualComposer::visual-composer.components.component-item')
</div>
@include('VisualComposer::visual-composer.components.component-selector')
@include('VisualComposer::visual-composer.components.modals.all-modals')
<script type="application/javascript" src="/vc-assets/js/dropzone.min.js"></script>
<script type="text/javascript">
    // Immediately after the js include
    Dropzone.autoDiscover = false;
</script>
<script>
    function addRowVC() {
        let container = document.querySelector(".vc-container > .vc-rows");
        let rowElem = document.querySelector(".vc-components .vc-row").cloneNode(true);
        rowElem.setAttribute("data-id", container.childElementCount + 1);
        container.appendChild(rowElem);

        initDragableRows();
        initDragableColumns();
        saveInfoVC();
    }

    function removeRowVC(elem) {
        elem.closest(".vc-row").remove();
        saveInfoVC();
    }

    function colStructureClick(elem) {

        let otherElems = elem.closest(".template-selector").querySelectorAll("li");
        for (let i = 0; i < otherElems.length; i++) {
            otherElems[i].classList.remove("active");
        }
        elem.classList.add("active");
        elem.closest(".btn-template-select").querySelector("span").innerHTML = elem.innerHTML;
        let colStructure = elem.getAttribute("data-structure");
        let colsSize = colStructure.split("-");
        let rowContainer = elem.closest(".vc-row").querySelector(".vc-row-container");
        let colCount = rowContainer.childElementCount;
        let newColCount = colsSize.length;
        let remainComponents = [];
        if (colCount > newColCount) {
            let allColumns = rowContainer.querySelectorAll(".vc-column");

            for (let i = newColCount; i < allColumns.length; i++) {
                allColumns[i].querySelectorAll(".vc-column-container .component-item").forEach(item => {
                    remainComponents.push(item);
                });
                allColumns[i].remove();
            }

            let rowColumns = rowContainer.querySelectorAll(".vc-column");
            let lastColumn = rowColumns[rowColumns.length - 1].querySelector(".vc-column-container");
            remainComponents.forEach(item => {
                lastColumn.append(item);
            })
        }
        let rowColumns = rowContainer.querySelectorAll(".vc-column");
        for (let i = 0; i < rowColumns.length; i++) {
            let columnContainer = rowColumns[i];
            columnContainer.setAttribute("data-id", i + 1);
            columnContainer.setAttribute("style", "width:" + parseInt(colsSize[i]) * 100 / 12 + "%");
        }
        for (let j = rowColumns.length; j < colsSize.length; j++) {
            let columnContainer = document.querySelector(".vc-components .vc-column").cloneNode(true);
            columnContainer.setAttribute("data-id", j + 1);
            columnContainer.setAttribute("style", "width:" + parseInt(colsSize[j]) * 100 / 12 + "%");
            rowContainer.append(columnContainer);

        }

        elem.closest(".vc-row").setAttribute("data-structure", colStructure);

        initDragableColumns();
        initDragableComponents();
        saveInfoVC();
    }

    function deleteComponent(elem) {
        elem.closest(".component-item").remove();
        saveInfoVC();
    }


    // Drag and Drop
    var drakeRow = null;
    var drakeColumn = null;
    var drakeComponent = null;

    function initDragableRows() {
        if (drakeRow != null) {
            drakeRow.destroy();
        }

        let containers = [document.querySelector(".vc-rows")];
        drakeRow = dragula(containers, {
            isContainer: function(el) {
                return false; // only elements in drake.containers will be taken into account
            },
            moves: function(el, source, handle, sibling) {
                if (handle.classList.contains("btn-vc-row-dragable")) {
                    return true; // elements are always draggable by default
                } else {
                    return false;
                }
                return true;

            },
            accepts: function(el, target, source, sibling) {

                return true; // elements can be dropped in any of the `containers` by default
            },
            invalid: function(el, handle) {
                return false; // don't prevent any drags from initiating by default
            },
            direction: 'vertical', // Y axis is considered when determining where an element would be dropped
            copy: false, // elements are moved by default, not copied
            copySortSource: false, // elements in copy-source containers can be reordered
            revertOnSpill: true, // spilling will put the element back where it was dragged from, if this is true
            removeOnSpill: false, // spilling will `.remove` the element, if this is true
            mirrorContainer: document.body, // set the element that gets mirror elements appended
            ignoreInputTextSelection: true, // allows users to select input text, see details below
            slideFactorX: 0, // allows users to select the amount of movement on the X axis before it is considered a drag instead of a click
            slideFactorY: 0, // allows users to select the amount of movement on the Y axis before it is considered a drag instead of a click
        });
        drakeRow.on("drop", () => {
            saveInfoVC();
        });
    }

    function initDragableColumns() {
        if (drakeColumn != null) {
            drakeColumn.destroy();
        }
        let containers = [];
        let listContainers = document.getElementsByClassName("vc-row-container");
        for (let i = 0; i < listContainers.length; i++) {
            containers.push(listContainers[i]);
        }

        drakeColumn = dragula(containers, {
            isContainer: function(el) {
                return false; // only elements in drake.containers will be taken into account
            },
            moves: function(el, source, handle, sibling) {
                if (handle.classList.contains("btn-vc-column-dragable")) {
                    return true; // elements are always draggable by default
                } else {
                    return false;
                }

            },
            accepts: function(el, target, source, sibling) {

                if (target.closest(".vc-row").getAttribute("data-id") == source.closest(".vc-row")
                    .getAttribute("data-id")) {
                    return true; // elements can be dropped in any of the `containers` by default
                } else {
                    return false;
                }
            },
            invalid: function(el, handle) {

                return false; // don't prevent any drags from initiating by default
            },
            direction: 'horizontal', // Y axis is considered when determining where an element would be dropped
            copy: false, // elements are moved by default, not copied
            copySortSource: false, // elements in copy-source containers can be reordered
            revertOnSpill: true, // spilling will put the element back where it was dragged from, if this is true
            removeOnSpill: false, // spilling will `.remove` the element, if this is true
            mirrorContainer: document.body, // set the element that gets mirror elements appended
            ignoreInputTextSelection: true, // allows users to select input text, see details below
            slideFactorX: 10, // allows users to select the amount of movement on the X axis before it is considered a drag instead of a click
            slideFactorY: 10, // allows users to select the amount of movement on the Y axis before it is considered a drag instead of a click
        });
        drakeColumn.on("drop", () => {
            saveInfoVC();
        });
    }

    function initDragableComponents() {
        if (drakeComponent != null) {
            drakeComponent.destroy();
        }
        let containers = [];
        let listContainers = document.getElementsByClassName("vc-column-container");
        for (let i = 0; i < listContainers.length; i++) {
            containers.push(listContainers[i]);
        }
        drakeComponent = dragula(containers, {
            isContainer: function(el) {
                return false; // only elements in drake.containers will be taken into account
            },
            moves: function(el, source, handle, sibling) {
                if (handle.classList.contains("column-empty-message")) {
                    return false; // elements are always draggable by default
                } else {
                    return true;
                }
            },
            accepts: function(el, target, source, sibling) {
                if (target.querySelector(".column-empty-message")) {
                    target.querySelector(".column-empty-message").remove();
                    target.style.display = "block";
                }
                if (source.childElementCount == 0) {
                    let message = document.createElement("span");
                    message.classList.add("column-empty-message");
                    message.innerHTML = "{{ __('visualcomposer::general.click_to_add_item') }}";
                    source.append(message);
                    source.style.display = "flex";
                }
                return true; // elements can be dropped in any of the `containers` by default
            },
            invalid: function(el, handle) {
                return false; // don't prevent any drags from initiating by default
            },
            direction: 'vertical', // Y axis is considered when determining where an element would be dropped
            copy: false, // elements are moved by default, not copied
            copySortSource: false, // elements in copy-source containers can be reordered
            revertOnSpill: true, // spilling will put the element back where it was dragged from, if this is true
            removeOnSpill: false, // spilling will `.remove` the element, if this is true
            mirrorContainer: document.body, // set the element that gets mirror elements appended
            ignoreInputTextSelection: true, // allows users to select input text, see details below
            slideFactorX: 0, // allows users to select the amount of movement on the X axis before it is considered a drag instead of a click
            slideFactorY: 0, // allows users to select the amount of movement on the Y axis before it is considered a drag instead of a click
        });
        drakeComponent.on("drop", () => {
            saveInfoVC();
        });
    }

    //save & load
    function saveInfoVC() {
        let vcContainer = document.querySelector(".vc-container");
        let rows = vcContainer.querySelectorAll(".vc-rows .vc-row");

        let rowsData = [];
        for (let i = 0; i < rows.length; i++) {
            let columns = rows[i].querySelectorAll(".vc-row-container .vc-column");
            let rowInfo = rows[i].querySelector("input[name='row-data']").value;
            if (rowInfo == "")
                rowInfo = "{}";
            rowInfo = JSON.parse(rowInfo);

            let rowStructure = rows[i].getAttribute("data-structure");

            let columnsData = [];
            for (let j = 0; j < columns.length; j++) {
                let components = columns[j].querySelectorAll(".vc-column-container .component-item");
                let columnWidth = columns[j].style.width;
                let componentsData = [];
                for (let k = 0; k < components.length; k++) {

                    let type = components[k].getAttribute("data-type");
                    let info = components[k].querySelector(".item-info input[name='component-data']").value;
                    if (info == "")
                        info = "{}"
                    info = JSON.parse(info);
                    componentsData.push({
                        type: type,
                        data: info
                    });
                }

                columnsData.push({
                    items: componentsData,
                    width: columnWidth
                });

            }

            rowsData.push({
                title: rowInfo.title ? rowInfo.title : "",
                direction: rowInfo.direction ? rowInfo.direction : "right",
                has_container: rowInfo.has_container ? rowInfo.has_container : false,
                has_line: rowInfo.has_line ? rowInfo.has_line : false,
                structure: rowStructure ? rowStructure : "12",
                bg_color: rowInfo.bg_color ? rowInfo.bg_color : "#ffffff",
                columns: columnsData
            });
        }
        document.querySelector("input[name='{{ $row->field }}']").setAttribute("value", JSON.stringify(rowsData));

        return rowsData;
    }

    function loadInfoVC() {
        let rawData = document.querySelector("input[name='{{ $row->field }}']").getAttribute("value");
        if (rawData == "") {
            rawData = "[]";
        }
        let data = JSON.parse(rawData);
        let container = document.querySelector(".vc-container > .vc-rows");
        container.innerHTML = "";
        for (let i = 0; i < data.length; i++) {
            let rowElem = document.querySelector(".vc-components .vc-row").cloneNode(true);
            rowElem.setAttribute("data-id", i + 1);
            let rowData = {
                ...data[i]
            };
            delete rowData.columns;
            rowElem.querySelector("input[name='row-data']").setAttribute("value", JSON.stringify(rowData));
            let rowContainer = rowElem.querySelector(".vc-row-container");
            rowContainer.innerHTML = '';

            rowElem.setAttribute("data-structure", data[i].structure);
            let otherSVGElems = rowElem.querySelectorAll(".btn-template-select ul li");
            for (let m = 0; m < otherSVGElems.length; m++) {
                otherSVGElems[m].classList.remove("active");
            }
            let structureIcon = rowElem.querySelector(".btn-template-select ul li[data-structure='" + data[i]
                .structure + "']");
            structureIcon.classList.add("active");
            rowElem.querySelector(".btn-template-select span").innerHTML = structureIcon.innerHTML;

            let columns = data[i].columns;
            for (let j = 0; j < columns.length; j++) {

                let colElem = document.querySelector(".vc-components .vc-column").cloneNode(true);
                let componentContainer = colElem.querySelector(".vc-column-container");

                colElem.setAttribute("data-id", j + 1);
                colElem.style.width = columns[j].width;
                let components = columns[j].items;
                for (let k = 0; k < components.length; k++) {

                    if (componentContainer.querySelector(".column-empty-message")) {
                        componentContainer.querySelector(".column-empty-message").remove();
                        componentContainer.style.display = "block";
                    }
                    let componentType = components[k].type;
                    let component = createComponentByType(componentType);
                    component.setAttribute("data-id", (i + 1) + "_" + (j + 1) + "_" + (k + 1));
                    component.querySelector(".item-info input[name='component-data']").setAttribute("value", JSON
                        .stringify(components[k].data));

                    componentContainer.append(component);
                }

                rowContainer.append(colElem);
            }
            container.appendChild(rowElem);
        }

        initDragableRows();
        initDragableColumns();
        initDragableComponents();
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        loadInfoVC();
    });

    let languageSwitcher = document.querySelectorAll(".language-selector .btn-group .btn-primary");
    if (languageSwitcher.length > 0) {
        languageSwitcher.forEach(lng => {
            lng.addEventListener("click", function() {
                setTimeout(() => {
                    loadInfoVC();
                }, 500);
            });
        })
    }
</script>
